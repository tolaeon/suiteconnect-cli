#!/bin/sh

# this depends on the linux netsuite odbc driver being installed, and the bitwarden cli with a configured vault

BATCH=$1
. /opt/netsuite/odbcclient/oaodbc64.sh
bw login --apikey

if [ "$(bw unlock --check)" != "Vault is unlocked!" ]; then
export BW_SESSION
BW_SESSION=$(bw unlock --raw)
fi

if [ -z "$BATCH" ]; then
  echo "batch arg is empty, start interactive mode"
  isql -v -n -m15 NetSuite "$(bw get username 'http://*.netsuite.com')" "$(bw get password 'http://*.netsuite.com')"
else
  sql -v -n -m15 NetSuite "$(bw get username 'http://*.netsuite.com')" "$(bw get password 'http://*.netsuite.com')" <<EOF 
${BATCH}
EOF
fi
