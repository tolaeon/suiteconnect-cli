# suiteconnect-cli

### Usage

search for a specific column of a table

`./odbc-client-connect.sh "help transactions" | grep -i business_unit`

Get the first 10 rows of a table
`./odbc-client-connect.sh "select top 10 * from transactions"`

### Configuration
